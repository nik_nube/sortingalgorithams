def findMinimumElementIndex(array, start, end):
    index = start
    for x in xrange(start + 1, end):
        if array[x] < array[index]:
            index = x
    return index

def unStableSelectionSort(array, start, end):
    for x in xrange(0, end - 1):
        index = findMinimumElementIndex(array, x, end)
        if index != x :
            array[x], array[index] = array[index], array[x]

def moveOneUnitToRight(array, start, end):
    for x in xrange(end, start, -1):
        array[x] = array[x - 1]

def stableSelectionSort(array, start, end):
    for x in xrange(0, end - 1):
        index = findMinimumElementIndex(array, x, end)
        if index != x :
            temp = array[index]
            moveOneUnitToRight(array, x, index)
            array[x] = temp
